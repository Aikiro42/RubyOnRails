class CreatePersonas < ActiveRecord::Migration[5.1]
  def change
    create_table :personas do |t|
      t.string :name
      t.text :description
      t.integer :age
      t.date :birthday
      t.string :nickname

      t.timestamps
    end
  end
end
