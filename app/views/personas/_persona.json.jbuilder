json.extract! persona, :id, :name, :description, :age, :birthday, :nickname, :created_at, :updated_at
json.url persona_url(persona, format: :json)
