class WelcomesController < ApplicationController	

	before_action :set_welcome, only:[:show,:edit,:update,:destroy]

	def new
		@welcome = Welcome.new
	end

	def index
		@welcome = Welcome.all()
	end

	def show

  end

	def create
		@welcome = Welcome.new(welcome_params)
  	@welcome.save
  	redirect_to @welcome
	end

	def destroy
		@welcome.destroy
		redirect_to '/'
	end

	def edit
		
	end

	def update
		@welcome.update(welcome_params)
	end

	private 
		def welcome_params
    	params.require(:welcome).permit(:title, :text)
  	end
		
		def set_welcome
			@welcome = Welcome.find(params[:id])
		end

end
